Building
--------

Here are some useful commands when working on the plugin; Make sure, you have run `mvn -DskipTests clean install` at
least once in the project's root directory to install the API and SPI into the local Maven repository. After that, run
the following commands.

Build the plugin from scratch and run the unit-tests:

    mvn clean package

Run unit and integration-tests:

    mvn verify

Run the integration-tests of specific test groups (see `atlassian-nav-links-plugin/pom.xml` for all available test groups, only one test group can run at a time):

    mvn verify -DtestGroups=jira-od -Pintegration-test -Dbaseurl=http://localhost:2990/jira

Run the plugin with a specific product (including building and unit-testing):

    mvn -Dproduct=jira amps:debug
    # or
    mvn -DinstanceId=jira amps:debug

Run one (!) group of apps (see `atlassian-nav-links-plugin/pom.xml` for all available test groups):

    mvn -DtestGroup=multiapp amps:run

Run pi from command line (redeploy plugin to a running instance, should work in theory):

    mvn -Dcontext.path=/jira -Dhttp.port=2990 amps:cli
    #--> enter 'pi'
